import requests, json, yaml, csv


def request (username, token):
    """
    Осуществляем запросы по username
    """
    headers = {'PRIVATE-TOKEN': '%s' % token}
    projects_list = list()

    projects_data = json.loads(requests.get(
        "https://gitwork.ru/api/v4/users/%s/projects.json" % username
        , headers = headers).text)


    for value in projects_data:
        projects_info = dict()
        projects_info['id'] = value['id']
        projects_info['name'] = value['name']
        projects_list.append(projects_info)

    projects_data.clear()

    for value in projects_list:
        projects_data = json.loads(requests.get(
            "https://gitwork.ru/api/v4/projects/%s/repository/commits" % value['id']
            , headers=headers).text)
        projects_info = dict()

        for vl in projects_data:
            if vl['committed_date'][0:10] in projects_info:
                projects_info[vl['committed_date'][0:10]] += 1
            else:
                projects_info[vl['committed_date'][0:10]] = 1
        value['commits'] = projects_info
    return projects_list




def main():

    with open(r'./config.yaml') as file:
        config = yaml.load(file, Loader=yaml.FullLoader)

    user_list = config['users_list']

    reader = csv.reader(open(user_list, 'r'), delimiter=';', quotechar='"')
    all_users = []
    for value in reader:
        user_info = dict()
        user_info['group'] = value[0]
        user_info['name'] = value[1]
        user_info['nickname'] = value[2]
        user_info['userdata'] = request(value[2], token=config['token'])
        all_users.append(user_info)

    print(all_users)

    ''' Мэйн '''

if __name__ == '__main__':
    main()