import csv
import json
import requests

def color(x):
    if x == 0:
        return 'c1'
    if x in range(1, 6):
        return 'c2'
    if x in range(6, 11):
        return 'c3'
    if x in range(11, 16):
        return 'c4'
    return 'c5'


def main():
    csv_f = open('file.csv', 'r')
    reader = csv.reader(csv_f, delimiter=';', quotechar='"')
    template = '<!DOCTYPE html><html>' \
               '<head>' \
               '<link rel="stylesheet" type="text/css" href="mystyle.css">' \
               '</head>' \
               '<title>TEST</title><body><div style="overflow-x:auto;"><table>{}</table></div></body></html>'
    data = ''
    users_data = dict()
    dates = set()

    for line in reader:
        udata = load_user(line[2])
        users_data[line[2]] = udata
        for date in udata.keys():
            dates.add(date)

    dates_s = sorted(list(dates))
    data += '<tr class="fixed"><th>users</th>{}</tr>' .format(''.join('<th>{}</th>'.format(x) for x in dates_s))
    for user, udata in users_data.items():
        data += "<tr><th>{}</th>{}</tr>".format(user, ''.join('<td class={}>{}</td>'.format(color(int(udata[x]) if x in udata else 0), udata[x] if x in udata else '-') for x in dates_s))
    with open('test2.html', 'w') as output_file:
        output_file.write(template.format(data))


def load_user(username):
    url = 'https://gitlab.com/users/%s/calendar.json' % username
    r = requests.get(url)
    d = json.loads(r.text)
    print(d)
    with open('test.html', 'a') as output_file:
        output_file.write(r.text)
    return d



if __name__ == '__main__':
    main()

